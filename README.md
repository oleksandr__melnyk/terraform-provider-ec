## Elastic Cloud provider

This repo contains source code for terraform provider: __terraform-provider-ec__.
https://www.elastic.co/guide/en/cloud/current/ec-restful-api.html

## Build

```sh
go build -v

# build using Docker container:
./build_docker.sh
```
## Environment variables

Open https://evbox.1password.com/
Get `elastic-api-key` secret
Set API key:
```sh
export EC_API_KEY = "Qwerty12345678"
```

## Provider

```sh

provider "ec" {
	url 	 = "https://api.elastic-cloud.com"
	api_key  = "Qwerty12345678"
	insecure = false
}

# Min usage:
provider "ec" {}
```
  - `url` - Elastic Cloud endpoint.
  - `api_key` - API Key. Set it with env var EC_API_KEY.
  - `insecure` - TLS setting for EC client.
## Data Source: ec_deployments

Use this data source to get names and IDs of existing deployments.
### Example usage

```sh

provider "ec" {}

data "ec_deployments" "this" {}
```
### Attributes Reference

  - `ids` - mapping `deployment name`: `id`

## Resource: ec_deployment
Manage ElasticCloud deployment

### Example usage

```sh

provider "ec" {}

resource "ec_deployment" "this" {
  name                = "my-first-api-deployment"
  region              = "gcp-europe-west1"
  version             = "7.8.0"
  template_id         = "gcp-io-optimized"

  data_node           = true
  master_node         = true
  ingest_node         = true
  ml_node             = false

  elastic_instance_id = "gcp.data.highio.1"
  elastic_zone_count  = 1
  elastic_node_memory = 8192

  kibana_instance_id = "gcp.kibana.1"
  kibana_zone_count  = 1
  kibana_node_memory = 1024
}

```
### Argument Reference

  - `name` - (Required) Deployment name.
  - `region` - (Required) Cloud region name. Depends on cloud provider.
  - `version` - (Required) Elastic stack version.
  - `template_id` - (Required) Unique identifier of the deployment template.
  - `data_node` - (Bool) Controls the combinations of Elasticsearch node types. Defines whether this node can hold data.
  - `master_node` - (Bool) Controls the combinations of Elasticsearch node types. Defines whether this node can be elected master.
  - `ingest_node` - (Bool) Controls the combinations of Elasticsearch node types. Defines whether this node can run an ingest pipeline.
  - `ml_node` - (Bool) Controls the combinations of Elasticsearch node types. Defines whether this node can run ml jobs.
  - `elastic_instance_id` - Instance ID for elastic node. Depends on cloud provider.
  - `elastic_zone_count` - Number of availability zones in selected region.
  - `elastic_node_memory` - Amount of memory (MB) per elastic node.
  - `kibana_instance_id` - Instance ID for kibana node. Depends on cloud provider.
  - `kibana_zone_count` - Number of availability zones in selected region.
  - `kibana_node_memory` - Amount of memory (MB) per kibana node.
  
### Attributes Reference
  - `username` - elasticsearch username.
  - `password` - elasticsearch password.
  - `endpoint` - cluster ednpoint.