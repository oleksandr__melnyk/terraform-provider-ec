package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

const (
	jsonContentType    = "application/json"
	deploymentResource = "/api/v1/deployments"
)

// ECClient struct contains HTTPClient, BaseURL, Key
type ECClient struct {
	HTTPClient *http.Client
	BaseURL    string
	Key        string
}

// ListDeployments function get
func (c *ECClient) ListDeployments() (resp *http.Response, err error) {
	log.Printf("[DEBUG] ListDeployments\n")

	resourceURL := c.BaseURL + deploymentResource
	authString := "ApiKey " + c.Key
	log.Printf("[DEBUG] ListDeployments Resource URL: %s\n", resourceURL)
	req, err := http.NewRequest("GET", resourceURL, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", jsonContentType)
	req.Header.Set("Authorization", authString)

	resp, err = c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}

	log.Printf("[DEBUG] ListDeployments response: %v\n", resp)

	return resp, nil
}

// GetDeployment returns deployment by id
func (c *ECClient) GetDeployment(id string) (resp *http.Response, err error) {
	log.Printf("[DEBUG] GetDeployment ID: %s\n", id)

	resourceURL := c.BaseURL + deploymentResource + "/" + id
	authString := "ApiKey " + c.Key
	log.Printf("[DEBUG] GetDeployment Resource URL: %s\n", resourceURL)
	req, err := http.NewRequest("GET", resourceURL, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", jsonContentType)
	req.Header.Set("Authorization", authString)

	resp, err = c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}

	log.Printf("[DEBUG] GetDeployment response: %v\n", resp)

	if resp.StatusCode != 200 && resp.StatusCode != 404 {
		respBytes, _ := ioutil.ReadAll(resp.Body)
		return nil, fmt.Errorf("%q: deploymnet could not be retrieved: %v", id, string(respBytes))
	}

	return resp, nil

}

// CreateDeployment function
func (c *ECClient) CreateDeployment(createDeploymentRequest DeploymentCreateRequest) (createResponse *DeploymentCreateResponse, err error) {
	log.Printf("[DEBUG] CreateDeployment: %v\n", createDeploymentRequest)
	jsonData, err := json.Marshal(createDeploymentRequest)

	if err != nil {
		return nil, err
	}

	jsonString := string(jsonData)
	log.Printf("[DEBUG] CreateDeployment request body: %s\n", jsonString)

	body := strings.NewReader(jsonString)
	resourceURL := c.BaseURL + deploymentResource
	authString := "ApiKey " + c.Key

	log.Printf("[DEBUG] CreateDeployment Resource URL: %s\n", resourceURL)
	req, err := http.NewRequest("POST", resourceURL, body)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", jsonContentType)
	req.Header.Set("Authorization", authString)

	resp, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}

	log.Printf("[DEBUG] CreateDeployment response: %v\n", resp)

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	log.Printf("[DEBUG] CreateDeployment response body: %v\n", string(respBytes))

	err = json.Unmarshal(respBytes, &createResponse)
	if err != nil {
		return nil, err
	}

	return createResponse, nil

}

