package main

import (
	"encoding/json"
	"io/ioutil"
	"log"

	"github.com/hashicorp/terraform/helper/schema"
)

/*

resource "ec_deployment" "this" {
  name                = "my-first-api-deployment"
  region              = "gcp-europe-west1"
  version             = "7.8.0"
  template_id         = "gcp-io-optimized"

  data_node           = true
  master_node         = true
  ingest_node         = true
  ml_node             = false

  elastic_instance_id = "gcp.data.highio.1"
  elastic_zone_count  = 1
  elastic_node_memory = 8192

  kibana_instance_id = "gcp.kibana.1"
  kibana_zone_count  = 1
  kibana_node_memory = 1024
}

*/
func resourceElasticsearchDeployment() *schema.Resource {
	return &schema.Resource{
		Create: resourceElasticsearchDeploymentCreate,
		Read:   resourceElasticsearchDeploymentRead,
		Update: resourceElasticsearchDeploymentUpdate,
		Delete: resourceElasticsearchDeploymentDelete,
		Schema: map[string]*schema.Schema{
			"elastic_instance_id": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				Description: "Elastic instance ID",
			},
			"elastic_zone_count": &schema.Schema{
				Type:        schema.TypeInt,
				Description: "Number cloud zones",
				Required:    true,
			},
			"elastic_node_memory": &schema.Schema{
				Type:        schema.TypeInt,
				Description: "Memory amount per node",
				Required:    true,
			},
			"kibana_instance_id": &schema.Schema{
				Type:        schema.TypeString,
				Computed:    false,
				Required:    true,
				Description: "kibana_instance_id",
			},
			"kibana_zone_count": &schema.Schema{
				Type:        schema.TypeInt,
				Description: "Number cloud zones",
				Required:    true,
			},
			"kibana_node_memory": &schema.Schema{
				Type:        schema.TypeInt,
				Description: "Defines memory amount per node",
				Required:    true,
			},
			"name": &schema.Schema{
				Type:        schema.TypeString,
				Description: "A name for the deployment; otherwise this will be the generated deployment id",
				ForceNew:    false,
				Required:    true,
			},
			"region": &schema.Schema{
				Type:        schema.TypeString,
				Description: "A name for the cloud region",
				ForceNew:    false,
				Required:    true,
			},
			"version": &schema.Schema{
				Type:        schema.TypeString,
				Description: "Elastic product version",
				ForceNew:    false,
				Required:    true,
			},
			"template_id": &schema.Schema{
				Type:        schema.TypeString,
				Description: "Template ID",
				ForceNew:    false,
				Required:    true,
			},
			"username": &schema.Schema{
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Cluster username",
			},
			"password": &schema.Schema{
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Cluster password",
			},
			"endpoint": &schema.Schema{
				Type:        schema.TypeString,
				Computed:    true,
				Description: "Cluster endpoint",
			},
			"data_node": &schema.Schema{
				Type:        schema.TypeBool,
				Description: "Elastic Data node",
				ForceNew:    false,
				Optional:    true,
				Default:     true,
			},
			"master_node": &schema.Schema{
				Type:        schema.TypeBool,
				Description: "Elastic Master node",
				ForceNew:    false,
				Optional:    true,
				Default:     true,
			},
			"ingest_node": &schema.Schema{
				Type:        schema.TypeBool,
				Description: "Elastic Ingest node",
				ForceNew:    false,
				Optional:    true,
				Default:     true,
			},
			"ml_node": &schema.Schema{
				Type:        schema.TypeBool,
				Description: "Elastic Machine Learning node",
				ForceNew:    false,
				Optional:    true,
				Default:     false,
			},
		},
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},
	}
}

func resourceElasticsearchDeploymentCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*ECClient)

	deploymentName := d.Get("name").(string)
	region := d.Get("region").(string)
	version := d.Get("version").(string)
	templateID := d.Get("template_id").(string)

	elasticInstanceID := d.Get("elastic_instance_id").(string)
	elasticZoneCount := d.Get("elastic_zone_count").(int)
	elasticNodeMemory := d.Get("elastic_node_memory").(int)

	kibanaInstanceID := d.Get("kibana_instance_id").(string)
	kibanaZoneCount := d.Get("kibana_zone_count").(int)
	kibanaNodeMemory := d.Get("kibana_node_memory").(int)

	dataNode := d.Get("data_node").(bool)
	masterNode := d.Get("master_node").(bool)
	ingestNode := d.Get("ingest_node").(bool)
	mlNode := d.Get("ml_node").(bool)

	log.Printf("[DEBUG] Creating deployment with name: %s\n", deploymentName)

	kibanaConfiguration := KibanaConfiguration{
		//		SystemSettings: KibanaSystemSettings{},
		Version: version,
	}
	kibanaSize := TopologySize{
		Resource: "memory",
		Value:    kibanaNodeMemory,
	}
	kibanaClusterTopology := KibanaClusterTopologyElement{
		InstanceConfigurationID: kibanaInstanceID,
		//		Kibana:                  KibanaConfiguration{},
		Size:      kibanaSize,
		ZoneCount: kibanaZoneCount,
	}

	kibanaClusterPlan := KibanaClusterPlan{
		ClusterTopology: []KibanaClusterTopologyElement{kibanaClusterTopology},
		Kibana:          kibanaConfiguration,
		//		Transient:       TransientKibanaPlanConfiguration{},
	}

	kibanaPayload := KibanaPayload{
		ElasticsearchClusterRefID: "elasticsearch",
		Plan:                      kibanaClusterPlan,
		RefID:                     "kibana",
		Region:                    region,
		//		Settings:                  KibanaClusterSettings{},
	}
	elasticsearchNodeType := ElasticsearchNodeType{
		Data:   dataNode,
		ML:     mlNode,
		Ingest: ingestNode,
		Master: masterNode,
	}
	topologySize := TopologySize{
		Resource: "memory",
		Value:    elasticNodeMemory,
	}

	clusterTopology := ElasticsearchClusterTopologyElement{
		//		Elasticsearch:           ElasticsearchConfiguration{},
		NodeType:                elasticsearchNodeType,
		InstanceConfigurationID: elasticInstanceID,
		ZoneCount:               elasticZoneCount,
		Size:                    topologySize,
	}
	elasticsearchConfiguration := ElasticsearchConfiguration{
		Version: version,
	}
	deploymentTemplate := DeploymentTemplateReference{
		ID: templateID,
	}

	elasticsearchClusterPlan := ElasticsearchClusterPlan{
		ClusterTopology:    []ElasticsearchClusterTopologyElement{clusterTopology},
		Elasticsearch:      elasticsearchConfiguration,
		DeploymentTemplate: deploymentTemplate,
		//		Transient:          TransientElasticsearchPlanConfiguration{},
	}

	elasticsearchPayload := ElasticsearchPayload{
		//		Settings: ElasticsearchClusterSettings{},
		Region: region,
		RefID:  "elasticsearch",
		Plan:   elasticsearchClusterPlan,
	}
	resources := DeploymentCreateResources{
		Elasticsearch: []ElasticsearchPayload{elasticsearchPayload},
		Kibana:        []KibanaPayload{kibanaPayload},
		//		Apm:           []ApmPayload{},
		//		Appsearch:     []AppSearchPayload{},
	}
	createDeploymentRequest := DeploymentCreateRequest{
		Name:      deploymentName,
		Resources: resources,
		//		Settings:  DeploymentCreateSettings{},
	}

	createResponse, err := client.CreateDeployment(createDeploymentRequest)
	if err != nil {
		return err
	}
	deploymentID := createResponse.ID

	log.Printf("[DEBUG] Created deployment ID: %s\n", deploymentID)

	d.SetId(deploymentID)
	d.Set("username", createResponse.Resources[0].Credentials.Username)
	d.Set("password", createResponse.Resources[0].Credentials.Password)

	return resourceElasticsearchDeploymentRead(d, meta)
}

func resourceElasticsearchDeploymentRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*ECClient)

	deploymentID := d.Id()
	log.Printf("[DEBUG] Reading deployment information for ID: %s\n", deploymentID)

	resp, err := client.GetDeployment(deploymentID)
	if err != nil {
		return err
	}

	if resp.StatusCode == 404 {
		log.Printf("[DEBUG] Deployment ID not found: %s\n", deploymentID)
		d.SetId("")
		return nil
	}

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	log.Printf("[DEBUG] Deployment response body: %v\n", string(respBytes))

	var deployInfo DeploymentGetResponse
	err = json.Unmarshal(respBytes, &deployInfo)
	if err != nil {
		return err
	}

	log.Printf("[DEBUG] Setting name: %v\n", deployInfo.Name)
	d.Set("deploy_name", deployInfo.Name)

	return nil
}

func resourceElasticsearchDeploymentUpdate(d *schema.ResourceData, meta interface{}) error {
	return nil
}

func resourceElasticsearchDeploymentDelete(d *schema.ResourceData, meta interface{}) error {
	return nil
}

