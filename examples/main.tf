provider "ec" {}

resource "ec_deployment" "this" {
  name                = "my-first-api-deployment"
  region              = "gcp-europe-west1"
  version             = "7.8.0"
  template_id         = "gcp-io-optimized"

  data_node           = true
  master_node         = true
  ingest_node         = true
  ml_node             = false

  elastic_instance_id = "gcp.data.highio.1"
  elastic_zone_count  = 1
  elastic_node_memory = 8192

  kibana_instance_id = "gcp.kibana.1"
  kibana_zone_count  = 1
  kibana_node_memory = 1024
}
